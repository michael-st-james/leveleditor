﻿using System.Text.RegularExpressions;
using UnityEngine;
using UnityEngine.UI;

namespace LevelEditor
{
    public class ConsoleView : MonoBehaviour
    {
        public ConsoleController console = new ConsoleController();

        bool didShow = false;

        public GameObject viewContainer; //Container for console view, should be a child of this GameObject
        public Text logTextArea;
        public InputField inputField;

        void Start()
        {
            if (console != null)
            {
                //console.visibilityChanged += onVisibilityChanged;
                console.logChanged += onLogChanged;
            }

            updateLogStr(console.log);
        }

        ~ConsoleView()
        {
            console.visibilityChanged -= onVisibilityChanged;
            console.logChanged -= onLogChanged;
        }

        private void Update()
        {
            var text = inputField.text;
            text = Regex.Replace(text, @"[^a-zA-Z0-9/ ]", "");
            inputField.text = text;

            //Toggle visibility when tilde key pressed
            if (Input.GetKeyUp("`"))
            {
                toggleVisibility();
            }

            //Toggle visibility when 5 fingers touch.
            if (Input.touches.Length == 5)
            {
                if (!didShow)
                {
                    toggleVisibility();
                    didShow = true;
                }
            }
            else
            {
                didShow = false;
            }
        }

        void toggleVisibility()
        {
            setVisibility(!viewContainer.activeSelf);
            inputField.Select();
            inputField.ActivateInputField();
        }

        void setVisibility(bool visible)
        {
            viewContainer.SetActive(visible);
        }

        void onVisibilityChanged(bool visible)
        {
            setVisibility(visible);
        }

        void onLogChanged(string[] newLog)
        {
            updateLogStr(newLog);
            inputField.Select();
            inputField.ActivateInputField();
        }

        void updateLogStr(string[] newLog)
        {
            if (newLog == null)
            {
                logTextArea.text = "";
            }
            else
            {
                logTextArea.text = string.Join("\n", newLog);
            }
        }

        /// 
        /// Event that should be called by anything wanting to submit the current input to the console.
        ///]
        public void RunCommand()
        {
            console.runCommandString(inputField.text);
            inputField.text = "";
        }
    }
}