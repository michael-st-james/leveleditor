﻿using System;
using System.IO;
using System.Linq;
using Level;
using Newtonsoft.Json;
using UnityEngine;
using Util.ResourceManagement;

namespace LevelEditor
{
    [Serializable]
    public class LevelEditor : MonoBehaviour
    {
        [SerializeField] private GameObject rootGo;

        [SerializeField] private string levelName;

        private ConsoleView _consoleView;
        private ConsoleController _consoleController;
        private LevelRoot _root;

        private void Awake()
        {
            _consoleView = GetComponentInChildren<ConsoleView>();
            _consoleController = _consoleView.console;
            _consoleController.SetLevelEditor(this);
        }

        private void Start()
        {
            // Stop time
            Time.timeScale = 0.00f;

            if (levelName.Length != 0)
            {
                LoadLevel();
            }

            if (_root == null)
            {
                Debug.Log("Instantiating new level root");
                rootGo = InsertLevelObject<LevelRoot>(out _root);
            }
        }

        private void OnApplicationQuit()
        {
            SaveLevel();
        }


        private void SaveLevel()
        {
            if (string.IsNullOrEmpty(levelName))
            {
                levelName = "new_file";
            }

            string levelFile = levelName + ".json";
            var level = new LevelData();
            var levelObjects = FindObjectsOfType<LevelObject>();
            foreach (var levelObject in levelObjects)
            {
                if (levelObject.transform.parent == null || levelObject.GetComponentInParent<LevelObject>() == null)
                {
                    level.levelObjects.Add(levelObject.data);
                }
            }

            string serializedLevel = JsonConvert.SerializeObject(level, Formatting.Indented,
                new JsonSerializerSettings()
                {
                    ReferenceLoopHandling = ReferenceLoopHandling.Ignore
                }
            );
            string saveFolder = Application.dataPath + "/Resources/Levels/";

            File.WriteAllText(saveFolder + levelFile, serializedLevel);
            Debug.Log("LevelEditor :: OnApplicationQuit :: Saved level data :: " + saveFolder + levelFile + "\n" +
                      serializedLevel);
        }

        private void LoadLevel()
        {
            string folder = Application.dataPath + "/Resources/Levels/";
            string levelFile;
            if (levelName.Length == 0)
                levelFile = "new_level.json";
            else
                levelFile = levelName + ".json";

            string path = Path.Combine(folder, levelFile);

            if (File.Exists(path))
            {
                var foundObjects =
                    FindObjectsOfType<LevelObject>();
                foreach (var obj in foundObjects)
                    Destroy(obj.gameObject);

                string json = File.ReadAllText(path);
                var level = JsonConvert.DeserializeObject<LevelData>(json);

                for (var i = 0; i < level.levelObjects.Count; i++)
                {
                    var levelObject = level.levelObjects.ElementAt(i);
                    Debug.Log(levelObject);
                    string prefabName = levelObject.prefabName;
                    if (string.IsNullOrEmpty(prefabName))
                    {
                        continue;
                    }

                    var go = InsertLevelObject(prefabName);
                }
            }
        }

        public GameObject InsertLevelObject<T>(out T prefab) where T : LevelObject
        {
            var go = Instantiate(AssetManager.GetEntity<T>());
            prefab = go.GetComponent<T>();
            prefab.SaveSerializationInfo(prefab.GetType().Name);
            return go;
        }

        public GameObject InsertLevelObject(string prefabName)
        {
            Debug.Log("Loaded level object: " + prefabName);
            var go = Instantiate(AssetManager.GetEntityWithName<LevelObject>(prefabName));
            if (go.GetComponent<LevelRoot>() != null)
            {
                _root = go.GetComponent<LevelRoot>();
                rootGo = go;
            }

            if (go.GetComponent<LevelObject>() != null)
            {
                go.GetComponent<LevelObject>().SaveSerializationInfo(prefabName);
            }

            return go;
        }
    }
}