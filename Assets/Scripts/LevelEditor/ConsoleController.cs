﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace LevelEditor
{
    public delegate void CommandHandler(string[] args);

    public class ConsoleController
    {

        private LevelEditor _levelEditor;
        
        #region Event declarations

        // Used to communicate with ConsoleView
        public delegate void LogChangedHandler(string[] log);

        public event LogChangedHandler logChanged;

        public delegate void VisibilityChangedHandler(bool visible);

        public event VisibilityChangedHandler visibilityChanged;

        #endregion

        /// 
        /// Object to hold information about each command
        /// 
        private class CommandRegistration
        {
            private string command { get; set; }
            public CommandHandler handler { get; private set; }
            private string help { get; set; }

            public CommandRegistration(string command, CommandHandler handler, string help)
            {
                this.command = command;
                this.handler = handler;
                this.help = help;
            }
        }

        /// 
        /// How many log lines should be retained?
        /// Note that strings submitted to appendLogLine with embedded newlines will be counted as a single line.
        /// 
        const int scrollbackSize = 20;

        Queue<string> scrollback = new Queue<string>(scrollbackSize);
        List<string> commandHistory = new List<string>();
        Dictionary<string, CommandRegistration> commands = new Dictionary<string, CommandRegistration>();

        public string[] log { get; private set; } //Copy of scrollback as an array for easier use by ConsoleView

        const string
            repeatCmdName =
                "!!"; //Name of the repeat command, constant since it needs to skip these if they are in the command history

        public ConsoleController()
        {
            //When adding commands, you must add a call below to registerCommand() with its name, implementation method, and help text.
            registerCommand("/echo", echo,
                "Example command that demonstrates how to parse arguments. echo [word] [# of times to repeat]");
            registerCommand("/add", add, "Add a level object. /add :prefab :x :z");
        }

        void registerCommand(string command, CommandHandler handler, string help)
        {
            commands.Add(command, new CommandRegistration(command, handler, help));
        }

        public void appendLogLine(string line)
        {
            Debug.Log(line);


            if (scrollback.Count >= scrollbackSize)
            {
                scrollback.Dequeue();
            }

            scrollback.Enqueue(line);

            log = scrollback.ToArray();
            if (logChanged != null)
            {
                logChanged(log);
            }
        }

        public void runCommandString(string commandString)
        {
            appendLogLine("$ " + commandString + "\n");

            var commandSplit = parseArguments(commandString);
            var args = new string[0];
            if (commandSplit.Length == 0 || commandString == null)
            {
                return;
            }
            else if (commandSplit.Length >= 2)
            {
                int numArgs = commandSplit.Length - 1;
                args = new string[numArgs];
                Array.Copy(commandSplit, 1, args, 0, numArgs);
            }

            runCommand(commandSplit[0].ToLower(), args);
            commandHistory.Add(commandString);
        }

        public void runCommand(string command, string[] args)
        {
            CommandRegistration reg = null;
            if (!commands.TryGetValue(command, out reg))
            {
                appendLogLine(string.Format("Unknown command '{0}', type 'help' for list." + "\n", command));
            }
            else
            {
                if (reg.handler == null)
                {
                    appendLogLine(string.Format("Unable to process command '{0}', handler was null." + "\n", command));
                }
                else
                {
                    reg.handler(args);
                }
            }
        }

        static string[] parseArguments(string commandString)
        {
            LinkedList<char> parmChars = new LinkedList<char>(commandString.ToCharArray());
            bool inQuote = false;
            var node = parmChars.First;
            while (node != null)
            {
                var next = node.Next;
                if (node.Value == '"')
                {
                    inQuote = !inQuote;
                    parmChars.Remove(node);
                }

                if (!inQuote && node.Value == ' ')
                {
                    node.Value = '\n';
                }

                node = next;
            }

            char[] parmCharsArr = new char[parmChars.Count];
            parmChars.CopyTo(parmCharsArr, 0);
            return (new string(parmCharsArr)).Split(new char[] {'\n'}, StringSplitOptions.RemoveEmptyEntries);
        }

        #region Command handlers

        //Implement new commands in this region of the file.

        /// 
        /// A test command to demonstrate argument checking/parsing.
        /// Will repeat the given word a specified number of times.
        /// 
        void echo(string[] args)
        {
            if (args.Length == 0)
            {
                return;
            }

            string word = args[0];

            int num = 1;
            if (args.Length > 1)
            {
                num = Convert.ToInt32(args[1]);
            }

            for (var i = 0; i < num; i++)
            {
                appendLogLine(word);
            }
        }

        void add(String[] args)
        {
            if (_levelEditor != null)
            {
                GameObject go = _levelEditor.InsertLevelObject(args[0]);

                if (args.Length >= 3)
                {
                    var xs = args[1];
                    var zs = args[2];

                    float x = float.Parse(xs);
                    float z = float.Parse(zs);
                    go.transform.position = new Vector3(x, 0, z);
                }
            }
        }

        void reload(string[] args)
        {
            Application.LoadLevel(Application.loadedLevel);
        }

        void resetPrefs(string[] args)
        {
            PlayerPrefs.DeleteAll();
            PlayerPrefs.Save();
        }

        #endregion

        public void SetLevelEditor(LevelEditor levelEditor)
        {
            this._levelEditor = levelEditor;
        }
    }
}