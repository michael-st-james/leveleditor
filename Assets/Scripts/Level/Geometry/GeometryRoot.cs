﻿using System;
using System.Runtime.InteropServices;
using UnityEngine;

namespace Level.Geometry
{
    [Serializable]
    public class GeometryRoot : LevelObject
    {
        protected override void Awake()
        {
            base.Awake();
            Debug.Log("Geometry :: Awake");
        }
    }
}