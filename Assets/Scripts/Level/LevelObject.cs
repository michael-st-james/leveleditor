﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace Level
{
    public class LevelObject : MonoBehaviour
    {
        [Serializable]
        public struct Data
        {
            public string name;
            public string prefabName;
            public Vector3 pos;
            public Quaternion rot;

        }
        
        public Data data;

        protected virtual void Awake()
        {
            var t = transform;
            data.pos = t.position;
            data.rot = t.rotation;
        }

        protected virtual void Start()
        {
            if (string.IsNullOrEmpty(data.prefabName))
            {
                name = GetType().Name;
            }
            else
            {
                name = data.prefabName;
            }
        }

        public void SaveSerializationInfo(string prefabName)
        {
            data.prefabName = prefabName;
        } 
    }
}