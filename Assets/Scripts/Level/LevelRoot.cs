﻿using System;
using System.Transactions;
using Level.Geometry;
using UnityEngine;

namespace Level
{
    [Serializable]
    public class LevelRoot : LevelObject
    {
        [SerializeField] private GameObject geometryGo;

        [SerializeField] private GeometryRoot geometry;
        
        protected override void Awake()
        {
            base.Awake();
            Debug.Log("LevelRoot :: Awake");
        }
    }
}
