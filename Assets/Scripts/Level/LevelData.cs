﻿using System.Collections.Generic;
using UnityEngine;

namespace Level
{
    public class LevelData
    {
        public List<LevelObject.Data> levelObjects = new List<LevelObject.Data>();
    }
}