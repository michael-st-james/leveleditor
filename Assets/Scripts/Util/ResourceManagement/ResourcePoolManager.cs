﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace Util.ResourceManagement
{
    public class ResourcePoolManager : MonoBehaviour
    {
        [SerializeField] public List<ResourcePoolInfo> resourcePoolInfos;
        private List<ResourcePool> _resourcePools;

        private void Awake()
        {
            AssetManager.SetResourcePoolManager(this);

            _resourcePools = new List<ResourcePool>();
            if (resourcePoolInfos == null)
            {
                Debug.LogWarning("No resource pools detected");
                return;
            }

            foreach (var t in resourcePoolInfos)
            {
                var go = new GameObject();
                go.transform.SetParent(this.transform);
                var resourcePool = go.AddComponent<ResourcePool>();
                resourcePool.SetPoolPrefab(t.poolPrefab);
                resourcePool.SetDepth(t.depth);
                _resourcePools.Add(resourcePool);
                go.name = t.poolPrefab.name + " Pool";
            }
        }

        public GameObject GetItem<T>()
        {
            GameObject go = null;
            for(int i = 0; i < _resourcePools.Count; i++)
            {
                if (_resourcePools[i].GetComponentInChildren<T>() != null)
                {
                    go = _resourcePools[i].GetItemFromPool();
                    go.SetActive(true);
                }
            }

            return go;
        }
        
        public void ReturnItem<T>(GameObject itemToReturn)
        {
            for(int i = 0; i < _resourcePools.Count; i++)
            {
                if (_resourcePools[i].GetComponentInChildren<T>() != null)
                {
                    _resourcePools[i].ReturnItemToPool(itemToReturn);
                }
            }
        }

        [Serializable]
        public class ResourcePoolInfo
        {
            [SerializeField] public GameObject poolPrefab;
            [SerializeField] public int depth;
        }
    }
}