﻿using System.Linq;
using UnityEngine;

namespace Util.ResourceManagement
{
    /**
     * The AssetLibrary is added to a Game Object in the scene. Any instantiable prefabs should be added to it in the editor.
     * A reference is added to the ResourceManager on Awake to make the AssetLibrary statically referenced.
     **/
    public class AssetLibrary : MonoBehaviour
    {
        public GameObject[] prefabs;

        private static bool _created = false;

        private void Awake()
        {
            if (!_created)
            {
                DontDestroyOnLoad(transform.gameObject);
                AssetManager.SetAssetLibrary(this);
                _created = true;
            }
            else
            {
                Destroy(this.gameObject);
            }
        }

        public GameObject GetPrefab<T>()
        {
            return (from t in prefabs
                let gameObject = t.GetComponent<T>()
                where gameObject != null
                select t).FirstOrDefault();
        }

        public GameObject GetPrefab<T>(string prefabName)
        {
            return (from t in prefabs
                let prefabGameObject = t.GetComponent<T>()
                where prefabGameObject != null && t.name == prefabName
                select t).FirstOrDefault();
        }
    }
}