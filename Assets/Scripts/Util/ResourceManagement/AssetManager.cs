﻿using System;
using System.Reflection;
using UnityEngine;

 namespace Util.ResourceManagement {

    /**
     * Allows for static reference to the Asset Library. The reference is set at startup by the Asset Library.
     * Will add other things like asset pools, configuration loading, file saving, etc.
     **/
    public static class AssetManager
    {

        #region AssetLibrary
        private static AssetLibrary _assetLibrary;

        public static void SetAssetLibrary(AssetLibrary library)
        {
            _assetLibrary = library;
        }

        public static GameObject GetEntity<T>()
        {
            return _assetLibrary.GetPrefab<T>();
        }

        public static GameObject GetEntityWithName<T>(string name)
        {
            return _assetLibrary.GetPrefab<T>(name);
        }
        
        #endregion

        #region ResourcePools
        private static ResourcePoolManager _resourcePoolManager;

        public static void SetResourcePoolManager(ResourcePoolManager poolManager)
        {
            _resourcePoolManager = poolManager;
        }

        public static GameObject GetItemFromPool<T>()
        {
            return _resourcePoolManager.GetItem<T>();
        }
        
        public static void ReturnItemToPool<T>(GameObject go)
        {
            _resourcePoolManager.ReturnItem<T>(go);
        }

        #endregion

        #region JSONloader

        // Not implemented yet

        #endregion

    }
}