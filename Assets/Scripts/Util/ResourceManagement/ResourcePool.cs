﻿using System.Collections.Generic;
using UnityEngine;

namespace Util.ResourceManagement
{
    public class ResourcePool : MonoBehaviour
    {
        [SerializeField] private GameObject poolPrefab;

        [SerializeField] private int depth;

        private List<GameObject> _usedObjects;
        private List<GameObject> _availableObjects;
        private static Vector3 _poolPosition = new Vector3(0, -10, 0);

        private void Awake()
        {
            _availableObjects = new List<GameObject>();
            _usedObjects = new List<GameObject>();
        }

        public void SetPoolPrefab(GameObject poolPrefab)
        {
            this.poolPrefab = poolPrefab;
        }

        public void SetDepth(int depth)
        {
            if(depth < 1)
            {
                Debug.LogWarning("Cannot set pool depth less than 1");
                return;
            }

            int change = depth - GetObjectCount();

            if(change < 0)
            {
                RemoveItemsFromPool(change);
                this.depth = depth;
            }
            else if(change > 0)
            {
                this.depth = depth;
                FillPool();
            }
        }

        public GameObject GetItemFromPool()
        {
            GameObject go = null;
            if(GetObjectCount() < 1)
            {
                Debug.LogWarning("There are no objects in the pool!");
            }

            if(_availableObjects.Count > 0)
            {
                go = _availableObjects[0];
                _availableObjects.Remove(go);
                _usedObjects.Add(go);
            }
            else
            {
                go = _usedObjects[0];
                _usedObjects.Remove(go);
                _usedObjects.Add(go);
                // Debug.LogWarning("No objects available in pool so recycling an existing object:" + go.name);
            }

            return go;
        }

        public void ReturnItemToPool(GameObject go)
        {
            if (_usedObjects.Contains(go)) {
                _usedObjects.Remove(go);
                _availableObjects.Add(go);
                go.transform.position = _poolPosition;
            }
            else
            {
                Debug.LogError("Returned an object to a pool it does not belong to.");
            }
        }
        
        private void FillPool()
        {
            while(depth > 0 && depth > GetObjectCount())
            {
                GameObject go = Instantiate(poolPrefab, _poolPosition , new Quaternion());
                _availableObjects.Add(go);
                go.transform.SetParent(this.transform);
            }
             
        }

        private void RemoveItemsFromPool(int itemsToRemove)
        {
            if (itemsToRemove > GetObjectCount())
            {
                Debug.LogError("Can not remove more items than the pool depth, toRemove:" + itemsToRemove + ", poolDepth: " + depth);
            }

            while (itemsToRemove > 0 && _availableObjects.Count > 0)
            { 
                GameObject go = _availableObjects[0];
                _availableObjects.Remove(go);
                Destroy(go);
                itemsToRemove--;
            }

            while(itemsToRemove > 0 && _usedObjects.Count > 0)
            {
                GameObject go = _usedObjects[0];
                _usedObjects.Remove(go);
                Destroy(go);
                itemsToRemove--;
            }           
        }

        private int GetObjectCount()
        {
            return _availableObjects.Count + _usedObjects.Count;
        }
    }
}