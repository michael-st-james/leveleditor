﻿using UnityEngine;
using UnityEngine.PlayerLoop;

namespace Util
{
    public class Calc
    {
        public static Vector3 ScreenToPoint(Vector3 origin, Camera camera, out RaycastHit outhit)
        {
            var invalid = new Vector3(-99999, -99999, -99999);
            var ray = camera.ScreenPointToRay(origin);
            if (Physics.Raycast(ray, out outhit))
            {
                return outhit.point;
            }
            return invalid;
        }
    }
}